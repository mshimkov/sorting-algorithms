# Sorting Algorithms 

## Bubble Sort
#### How it works:
Bubble Sort is considered to be the simplest sorting algorithm that works by repeatedly swapping the adjacent elements if they are in the wrong order.

#### Performance:
| Case  | Big O|
| ------------- | ------------- |
| Worst Case  	| O(n^2)  |
| Average Case	| O(n^2)  |
| Best Case   	| O(n) |
| Space Required | 1 |

## Insertion Sort
#### How it works:
Insertion  Sort is a sorting algorithm that places the input elements at its suitable place in each pass. It works in the same 
way as we sort cards while playing cards. 

#### Performance:
| Case  | Big O|
| ------------- | ------------- |
| Worst Case  	| O(n^2)  |
| Average Case	| O(n^2)  |
| Best Case   	| O(n) |
| Space Required | 1 |

## Quick Sort
#### How it works:
Quick Sort is a sorting algorithm that recursively partitions the array into smaller partitions and arranges smaller and larger elements depending on a chosen input. 

#### Performance:
| Case  | Big O|
| ------------- | ------------- |
| Worst Case  	| O(n^2)  |
| Average Case	| O(n log n)  |
| Best Case   	| O(n log n) |
| Space Required | 1 |

## Merge Sort
#### How it works:
Merge Sort is a divide and conquer sorting algorithm.
#### Performance:
| Case  | Big O|
| ------------- | ------------- |
| Worst Case  	| O(n log n)  |
| Average Case	| O(n log n)  |
| Best Case   	| O(n log n) |
| Space Required | n |


