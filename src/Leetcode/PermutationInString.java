package Leetcode;

import java.util.Arrays;

/** Given two strings s1 and s2, write a function to return true if s2 contains the permutation of s1.
 *  In other words, one of the first string's permutations is the substring of the second string.
 */

public class PermutationInString {
    public static void main(String[] args) {
        String s1 = "oo";
        String s2 = "eidbaaoo";
        System.out.println(checkInclusion(s1, s2));
    }

    public static boolean checkInclusion(String s1, String s2) {
        s1 = sort(s1);
        for (int i = 0; i <= s2.length() - s1.length(); i++) {
            String temp = sort(s2.substring(i, i + s1.length()));
            System.out.println("i " + i + "," + (i + s1.length()));
            System.out.println(temp);
            if (s1.equals(temp))
                return true;
        }
        return false;
    }
    public static String sort(String s) {
        char[] t = s.toCharArray();
        Arrays.sort(t);
        return new String(t);
    }
}