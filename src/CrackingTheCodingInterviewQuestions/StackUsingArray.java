package CrackingTheCodingInterviewQuestions;

/** A stack is last in, first out data structure. So something like dishes.*/
public class StackUsingArray {
    public static void main(String[] args) {
        MyStack myStack = new MyStack(3);
        myStack.push(-10);
        myStack.push(3);
        myStack.push(-2);
        myStack.getMin();
    }
}


/** A stack array can be created from int arrays,
 * It has a max size, and top
 * */
class MyStack {
    private int[] stackArray;
    private int top;
    private int size;

    public MyStack(int size) {
        stackArray = new int[size];
        this.size = size;
        top = -1;
    }

    public int pop(){
        return stackArray[top--];
    }

    /** Add data point to the stack */
    public void push(int data){
        stackArray[++top] = data;
    }

    /** return top  */
    public int peek(){
        if( top < 0 ) {
            System.out.println("Stack is empty");
            return -1;
        }
        System.out.println(stackArray[top]);
        return stackArray[top];
    }

    /** return true if it is empty and false if it is not empty */
    public boolean isEmpty(){
        return stackArray.length == 0;
    }

    public int size(){
        return size;
    }

    public void getMin() {
        Integer min = Integer.MAX_VALUE;
        // go through the stack and look for min
        for (int i = 0; i < size() ; i++) {
            if (stackArray[i] < min) {
                min = stackArray[i];
            }
        }
        System.out.println(min);
    }
}
