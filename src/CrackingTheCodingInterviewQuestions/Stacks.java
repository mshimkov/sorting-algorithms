package CrackingTheCodingInterviewQuestions;
import java.util.EmptyStackException;

public class Stacks {

    public static void main(String[] args) {
        MyStacks<Integer> stack = new MyStacks<>();
        stack.push(9);
        stack.push(10);
        stack.peek();
        System.out.println(stack.peek());
    }
}

class MyStacks<T> {
    private StackNode<T> top;

    public class StackNode<T> {
        private T data;
        private StackNode<T> next;

        public StackNode(T data) {
            this.data = data;
            next = null;
        }
    }

    /** Remove your node from the stack */
    public T pop() {
        if (top == null) throw new EmptyStackException();
        T data = top.data;
        top = top.next;
        return data;
    }

    /** Return the top element */
    public T peek() {
        return top.data;
    }

    /**  check if it is empty */
    public boolean isEmpty() {
        return top == null;
    }

    /** Add your node to the stack */
    public void push(T data) {
        //make a new node
        StackNode<T> newNode = new StackNode<T>(data);
        newNode.next = newNode;
        top = newNode;
    }


}
