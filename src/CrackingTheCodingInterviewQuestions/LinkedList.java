package CrackingTheCodingInterviewQuestions;

public class LinkedList {
    public static void main(String[] args) {
        Mylist l = new Mylist();
        for (int i = 0; i < 10; i++) {
            l.appendData(i);
            System.out.println(i);
        }
        Mylist newList = new Mylist();
        System.out.println(newList.returnHead());
        l.reversedLinkedListRecursion(l.returnHead());
    }
}

class Mylist {
    private Node head;

    public Node returnHead(){
        return this.head;
    }

    public class Node {
        int data;
        Node next;

        public Node(int data) {
            this.data = data;
        }
    }

    public Node appendData(int data) {
        // make a new node
        Node newNode = new Node(data);
        newNode.next = null;

        if (head == null) {
            head = newNode;
        } else {
            Node last = head;
            while (last.next != null) {
                last = last.next;
            }

            last.next = newNode;
        }
        return newNode;
    }

    public void reversedLinkedListRecursion(Node head) {
        if(head == null) return;
        reversedLinkedListRecursion(head.next);
        System.out.println(head.data);
    }
}